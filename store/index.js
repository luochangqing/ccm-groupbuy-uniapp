import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		hasLogin: false,	//是否登录
		hasMerchant: false,	//是否选择自提点
		userInfo: {},		//登录用户信息
		merchantInfo: {},	//团长信息
		cartInfo: [],	//购物车信息
		footPrint: [],		//商品浏览历史
		applicationConfig:{},	//应用全局设置
		uiConfig:{	//UI全局设置, 
			baseColor: '#ff0000',
			activeLayoutName: 'layoutA',
			layoutList:[	//支持的布局列表
				{
					name: 'layoutA',
					fabDisplayedHome: true,	//首页是否显示右下角的圆形按钮(true或者false)
					sCols: 1,		//首页秒杀专栏所占列数(最多2列)
					sProductCols:2,//首页秒杀专栏一行显示多少个商品(最多4个)
					fCols: 1,		//首页商品组所占列数(最多2列)
					productCols:2	, //商品列表一行显示多少个商品(1或者2),影响所有除秒杀以为所有的商品列表页
				},
				{
					name: 'layoutB',
					fabDisplayedHome: false,	//首页是否显示右下角的圆形按钮(true或者false)
					sCols: 2,		//首页秒杀专栏所占列数(最多2列)
					sProductCols:4,//首页秒杀专栏一行显示多少个商品(最多4个)
					fCols: 2,		//首页商品组所占列数(最多2列)
					productCols:2	, //商品列表一行显示多少个商品(1或者2),影响所有除秒杀以为所有的商品列表页
				}
			],
		}
	},
	mutations: {
		addFootPrint(state, provider) {
			var oldFootPrint = state.footPrint;
			var footPrint = [];
			for(var i=0;i<oldFootPrint.length;i++){
				if(oldFootPrint[i].productUuid!=provider.productUuid){
					footPrint.push(oldFootPrint[i]);
				}
			}
			footPrint.push(provider);
			//只保留20个商品浏览历史
			if(footPrint.length>20){
				footPrint = footPrint.slice(-20);
			}
			state.footPrint = footPrint;
			uni.setStorage({//缓存应用全局设置
				key: 'footPrint',  
				data: footPrint  
			})
		},
		updateApplicationConfig(state, provider) {
			state.applicationConfig = provider;
			uni.setStorage({//缓存应用全局设置
			    key: 'applicationConfig',  
			    data: provider  
			}) 
		},
		updateActiveLayout(state, provider) {
			var activeLayoutName = provider;
			var uiConfig = state.uiConfig;
			var layoutList = uiConfig.layoutList;
			layoutList.forEach(function(layout,index){
				if(layout.name == activeLayoutName)
					uiConfig.activeLayout = layout
			})
			uni.setStorage({//缓存应用全局设置
				key: 'uiConfig',  
				data: uiConfig  
			}) 
		},
		updateUserInfo(state, provider) {
			state.userInfo = provider;
			uni.setStorage({
			    key: 'userInfo',  
			    data: provider  
			}) 
		},
		login(state, provider) {
			state.hasLogin = true;
			state.userInfo = provider;
			uni.setStorage({//缓存用户登陆状态
			    key: 'userInfo',  
			    data: provider  
			}) 
			console.log(state.userInfo);
		},
		changeMerchant(state, provider) {
			state.hasMerchant = true;
			state.merchantInfo = provider;
			uni.setStorage({//缓存选择的团长
			    key: 'merchantInfo',  
			    data: provider  
			}) 
			console.log(state.merchantInfo);
		},
		updateCartInfo(state, provider) {
			state.cartInfo = provider;
			uni.setStorage({//缓存购物车数据
			    key: 'cartInfo',  
			    data: provider  
			}) 
			console.log(state.cartInfo);
		},
		logout(state) {
			state.hasLogin = false;
			state.userInfo = {};
			state.cartInfo = [];
			uni.removeStorage({  
                key: 'userInfo'  
            })
			uni.removeStorage({
			    key: 'userToken'  
			})
			uni.removeStorage({
			    key: 'cartInfo'  
			})
		}
	},
	actions: {
	
	}
})

export default store
